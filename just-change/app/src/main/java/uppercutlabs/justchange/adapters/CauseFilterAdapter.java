package uppercutlabs.justchange.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.List;

import uppercutlabs.justchange.R;
import uppercutlabs.justchange.models.CauseFilter;

/**
 * Created by hp1 on 3/15/2016.
 */
public class CauseFilterAdapter extends RecyclerView.Adapter<CauseFilterAdapter.CauseFilterViewHolder> {

    List<CauseFilter> listCauses;
    Context context;

    public CauseFilterAdapter(List<CauseFilter> listCauses, Context context) {
        this.listCauses = listCauses;
        this.context = context;
    }

    @Override
    public CauseFilterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View cvh = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.filter_item, parent, false);

        return new CauseFilterViewHolder(cvh);
    }

    @Override
    public void onBindViewHolder(final CauseFilterViewHolder holder, int position) {
        CauseFilter cause = listCauses.get(position);

        holder.tvName.setText(cause.getName());

        holder.rlCause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.tvName.isChecked()) {
                    holder.tvName.setChecked(false);
                } else {
                    holder.tvName.setChecked(true);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return listCauses.size();
    }

    class CauseFilterViewHolder extends RecyclerView.ViewHolder {
        public CheckedTextView tvName;
        public ImageView civProfile;
        public RelativeLayout rlCause;

        public CauseFilterViewHolder(View view) {
            super(view);
            tvName = (CheckedTextView) view.findViewById(R.id.checkedTextView);
            rlCause = (RelativeLayout) view.findViewById(R.id.filterLayout);
            //set image url to imageview here
        }
    }
}
