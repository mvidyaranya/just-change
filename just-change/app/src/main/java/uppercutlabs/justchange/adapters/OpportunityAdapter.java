package uppercutlabs.justchange.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import uppercutlabs.justchange.R;
import uppercutlabs.justchange.activities.CauseProfileActivity;
import uppercutlabs.justchange.activities.InternProfileActivity;
import uppercutlabs.justchange.models.Event;
import uppercutlabs.justchange.models.Opportunity;

/**
 * Created by hp1 on 3/22/2016.
 */
public class OpportunityAdapter  extends RecyclerView.Adapter<OpportunityAdapter.OpportunityViewHolder> {

    List<Opportunity> listOpportunities;
    Context context;
    private int lastPosition = -1;

    public OpportunityAdapter(List<Opportunity> listOpportunities, Context context) {
        this.listOpportunities = listOpportunities;
        this.context = context;
    }

    @Override
    public OpportunityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View ovh = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_opportunity, parent, false);

        return new OpportunityViewHolder(ovh);
    }

    @Override
    public void onBindViewHolder(OpportunityViewHolder holder, int position) {
        Opportunity opportunity = listOpportunities.get(position);

        holder.tvName.setText(opportunity.getName());
        holder.tvPlace.setText(opportunity.getPlace());
        holder.tvNeeds.setText(opportunity.getNeeds());
        holder.tvDescription.setText(opportunity.getDescription());
        holder.tvNotificationCount.setText(""+opportunity.getNotificationCount());

        holder.rlOpportunity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent causeProfile = new Intent(context, InternProfileActivity.class);
                context.startActivity(causeProfile);
            }
        });

     //   setAnimation(holder.rlOpportunity, position);
    }

    @Override
    public int getItemCount() {
        return listOpportunities.size();
    }

    private void setAnimation(View viewToAnimate, int position) {
        Animation animation = AnimationUtils.loadAnimation(context,
                (position > lastPosition) ? R.anim.up_from_bottom
                        : R.anim.down_from_top);
        viewToAnimate.startAnimation(animation);
        lastPosition = position;
    }

    class OpportunityViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName, tvPlace, tvNotificationCount, tvNeeds, tvDescription;
        public ImageView civProfile;
        public RelativeLayout rlOpportunity;

        public OpportunityViewHolder(View view) {
            super(view);
            tvName = (TextView) view.findViewById(R.id.tvOpportunityName);
            tvPlace = (TextView) view.findViewById(R.id.tvOpportunityPlace);
            tvNotificationCount = (TextView) view.findViewById(R.id.tvOpportunityNotificationCount);
            tvNeeds = (TextView) view.findViewById(R.id.tvOpportunityNeeds);
            tvDescription = (TextView) view.findViewById(R.id.tvOpportunityDescription);
            rlOpportunity = (RelativeLayout) view.findViewById(R.id.rlOpportunity);
            //set image url to imageview here
        }
    }
}

