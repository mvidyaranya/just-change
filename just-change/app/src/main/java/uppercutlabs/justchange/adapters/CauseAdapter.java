package uppercutlabs.justchange.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import uppercutlabs.justchange.R;
import uppercutlabs.justchange.activities.CauseProfileActivity;
import uppercutlabs.justchange.models.Cause;

/**
 * Created by hp1 on 3/15/2016.
 */
public class CauseAdapter extends RecyclerView.Adapter<CauseAdapter.CauseViewHolder> {

    List<Cause> listCauses;
    Context context;
    private int lastPosition = -1;

    public CauseAdapter(List<Cause> listCauses, Context context) {
        this.listCauses = listCauses;
        this.context = context;
    }

    @Override
    public CauseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View cvh = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_cause, parent, false);

        return new CauseViewHolder(cvh);
    }

    @Override
    public void onBindViewHolder(CauseViewHolder holder, int position) {
        Cause cause = listCauses.get(position);

        holder.tvName.setText(cause.getName());
        holder.tvDescription.setText(cause.getDescription());
        holder.tvNotificationCount.setText("" + cause.getNotificationCount());

        holder.rlCause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent causeProfile = new Intent(context, CauseProfileActivity.class);
                context.startActivity(causeProfile);
            }
        });

  //      setAnimation(holder.rlCause, position);
    }

    @Override
    public int getItemCount() {
        return listCauses.size();
    }

    private void setAnimation(View viewToAnimate, int position) {
        Animation animation = AnimationUtils.loadAnimation(context,
                (position > lastPosition) ? R.anim.up_from_bottom
                        : R.anim.down_from_top);
        viewToAnimate.startAnimation(animation);
        lastPosition = position;
    }


    class CauseViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName, tvDescription, tvNotificationCount;
        public ImageView civProfile;
        public RelativeLayout rlCause;

        public CauseViewHolder(View view) {
            super(view);
            tvName = (TextView) view.findViewById(R.id.tvCauseName);
            tvNotificationCount = (TextView) view.findViewById(R.id.tvCauseNotificationCount);
            tvDescription = (TextView) view.findViewById(R.id.tvCauseDescription);
            rlCause = (RelativeLayout) view.findViewById(R.id.rlCause);
            //set image url to imageview here
        }
    }
}
