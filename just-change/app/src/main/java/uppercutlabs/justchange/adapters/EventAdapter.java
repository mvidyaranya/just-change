package uppercutlabs.justchange.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import uppercutlabs.justchange.R;
import uppercutlabs.justchange.activities.CauseProfileActivity;
import uppercutlabs.justchange.activities.VolunteerProfileActivity;
import uppercutlabs.justchange.models.Cause;
import uppercutlabs.justchange.models.Event;

/**
 * Created by hp1 on 3/21/2016.
 */
public class EventAdapter extends RecyclerView.Adapter<EventAdapter.EventViewHolder> {

    List<Event> listEvents;
    Context context;
    private int lastPosition = -1;

    public EventAdapter(List<Event> listEvents, Context context) {
        this.listEvents = listEvents;
        this.context = context;
    }

    @Override
    public EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View cvh = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_event, parent, false);

        return new EventViewHolder(cvh);
    }

    @Override
    public void onBindViewHolder(EventViewHolder holder, int position) {
        Event event = listEvents.get(position);

        holder.tvName.setText(event.getName());
        holder.tvPlace.setText(event.getPlace());
        holder.tvDate.setText(event.getDate());
        holder.tvNotificationCount.setText(""+event.getNotificationCount());

        holder.rlEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent causeProfile = new Intent(context, VolunteerProfileActivity.class);
                context.startActivity(causeProfile);
            }
        });

     //   setAnimation(holder.rlEvent, position);
    }

    @Override
    public int getItemCount() {
        return listEvents.size();
    }

    private void setAnimation(View viewToAnimate, int position) {
        Animation animation = AnimationUtils.loadAnimation(context,
                (position > lastPosition) ? R.anim.up_from_bottom
                        : R.anim.down_from_top);
        viewToAnimate.startAnimation(animation);
        lastPosition = position;
    }

    class EventViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName, tvPlace, tvNotificationCount, tvDate;
        public ImageView civProfile;
        public RelativeLayout rlEvent;

        public EventViewHolder(View view) {
            super(view);
            tvName = (TextView) view.findViewById(R.id.tvEventName);
            tvPlace = (TextView) view.findViewById(R.id.tvEventPlace);
            tvNotificationCount = (TextView) view.findViewById(R.id.tvEventNotificationCount);
            tvDate = (TextView) view.findViewById(R.id.tvEventDate);
            rlEvent = (RelativeLayout) view.findViewById(R.id.rlEvent);
            //set image url to imageview here
        }
    }
}
