package uppercutlabs.justchange.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import uppercutlabs.justchange.R;
import uppercutlabs.justchange.models.CauseFilter;
import uppercutlabs.justchange.adapters.CauseFilterAdapter;


/**
 * A simple {@link Fragment} subclass.
 */
public class FilterFragment extends Fragment {

    RecyclerView rvCauses;
    LinearLayoutManager llManager;
    List<CauseFilter> listCauses;
    CauseFilterAdapter causeAdapter;

    public FilterFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        llManager = new LinearLayoutManager(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_filter, container, false);
        rvCauses = (RecyclerView) v.findViewById(R.id.listFilter);
        rvCauses.setLayoutManager(llManager);

        //set up dummy list
        initDummyCauses();
        causeAdapter = new CauseFilterAdapter(listCauses, getActivity());
        rvCauses.setAdapter(causeAdapter);
        return v;
    }


    private void initDummyCauses(){
        listCauses = new ArrayList<>();

        listCauses.add(new CauseFilter("imageUrl", "Cause Name"));
        listCauses.add(new CauseFilter("imageUrl", "Cause Name"));
        listCauses.add(new CauseFilter("imageUrl", "Cause Name"));
        listCauses.add(new CauseFilter("imageUrl", "Cause Name"));
        listCauses.add(new CauseFilter("imageUrl", "Cause Name"));
        listCauses.add(new CauseFilter("imageUrl", "Cause Name"));
        listCauses.add(new CauseFilter("imageUrl", "Cause Name"));
        listCauses.add(new CauseFilter("imageUrl", "Cause Name"));
        listCauses.add(new CauseFilter("imageUrl", "Cause Name"));


    }


}
