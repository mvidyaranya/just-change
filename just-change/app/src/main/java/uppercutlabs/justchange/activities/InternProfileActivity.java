package uppercutlabs.justchange.activities;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import uppercutlabs.justchange.R;
import uppercutlabs.justchange.fragments.AddressFragment;
import uppercutlabs.justchange.fragments.MapFragment;

public class InternProfileActivity extends AppCompatActivity {

    private ViewPager pager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intern_cause_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab6);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:9160242333")));
            }
        });

        FloatingActionButton fab1 = (FloatingActionButton) findViewById(R.id.fab5);
        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in=new Intent(Intent.ACTION_VIEW);
                in.setData(Uri.parse("sms:"));
                in.putExtra("sms_body", "Just Change");
                startActivity(in);
            }
        });

        pager = (ViewPager) findViewById(R.id.viewPager);
        pager.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));
    }

    public void intern(View v){
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_internship);
        dialog.setTitle("Internship details");

        Button dialogButton = (Button) dialog.findViewById(R.id.btnIntern);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(InternProfileActivity.this, ConfirmInternshipActivity.class));
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    private class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int pos) {
            switch (pos) {

                case 0:
                    return new AddressFragment();
                case 1:
                    return new MapFragment();

                default:
                    return new AddressFragment();
            }
        }

        @Override
        public float getPageWidth(int position) {
            if (position == 0) {
                return 0.70f;
            } else
                return 1.0f;
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}
