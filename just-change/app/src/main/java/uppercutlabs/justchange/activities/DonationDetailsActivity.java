package uppercutlabs.justchange.activities;

import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import uppercutlabs.justchange.R;

public class DonationDetailsActivity extends AppCompatActivity {

    CheckBox cbCash;
    TextInputLayout tilCash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donation_details);

        initViews();


        cbCash.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    tilCash.setVisibility(View.VISIBLE);
                }else{
                    tilCash.setVisibility(View.GONE);
                }
            }
        });
    }

    private void initViews() {
        cbCash = (CheckBox) findViewById(R.id.cbCash);
        tilCash = (TextInputLayout) findViewById(R.id.tilCash);
    }
}
