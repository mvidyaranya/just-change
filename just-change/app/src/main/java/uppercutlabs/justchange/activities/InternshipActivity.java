package uppercutlabs.justchange.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;

import com.gordonwong.materialsheetfab.MaterialSheetFab;

import java.util.ArrayList;
import java.util.List;

import uppercutlabs.justchange.R;
import uppercutlabs.justchange.adapters.EntryAdapter;
import uppercutlabs.justchange.models.Event;
import uppercutlabs.justchange.models.Opportunity;

import uppercutlabs.justchange.adapters.OpportunityAdapter;
import uppercutlabs.justchange.utils.EntryItem;
import uppercutlabs.justchange.utils.Fab;
import uppercutlabs.justchange.utils.Item;
import uppercutlabs.justchange.utils.SectionItem;

public class InternshipActivity extends AppCompatActivity {

    RecyclerView rvOpportunities;
    LinearLayoutManager llManager;
    OpportunityAdapter opportunityAdapter;
    List<Opportunity> listOpportunity;
    private MaterialSheetFab<Fab> materialSheetFab;
    ArrayList<Item> items = new ArrayList<Item>();
    ListView listview=null;

    private Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_internship);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.logo);



        llManager = new LinearLayoutManager(this);

        initViews();

        //set up dummy list
        initDummyOpportunities();
        opportunityAdapter = new OpportunityAdapter(listOpportunity, this);
        rvOpportunities.setLayoutManager(llManager);
        rvOpportunities.setAdapter(opportunityAdapter);

        Fab fab = (Fab) findViewById(R.id.fab);
        View sheetView = findViewById(R.id.fab_sheet);
        View overlay = findViewById(R.id.overlay);
        int sheetColor = getResources().getColor(R.color.white);
        int fabColor = getResources().getColor(R.color.colorAccentIntern);

        // Initialize material sheet FAB
        materialSheetFab = new MaterialSheetFab<>(fab, sheetView, overlay,
                sheetColor, fabColor);

        listview=(ListView)findViewById(R.id.listView_main);

        items.add(new SectionItem("Donate", R.drawable.icon18));
        items.add(new EntryItem("Toys"));
        items.add(new EntryItem("Clothes"));
        items.add(new EntryItem("Cash"));

        items.add(new SectionItem("To", R.drawable.icon19));
        items.add(new EntryItem("Children"));
        items.add(new EntryItem("Senior Citizen"));
        items.add(new EntryItem("Citizen"));

        EntryAdapter entryAdapter= new EntryAdapter(this, items);
        listview.setAdapter(entryAdapter);
    }

    public void initDummyOpportunities(){
        listOpportunity = new ArrayList<>();

        listOpportunity.add(new Opportunity("", "Name of the NGO", "Place", "Needs", "Description about the opportunity", 3));
        listOpportunity.add(new Opportunity("", "Name of the NGO", "Place", "Needs", "Description about the opportunity", 3));
        listOpportunity.add(new Opportunity("", "Name of the NGO", "Place", "Needs", "Description about the opportunity", 3));
        listOpportunity.add(new Opportunity("", "Name of the NGO", "Place", "Needs", "Description about the opportunity", 3));
        listOpportunity.add(new Opportunity("", "Name of the NGO", "Place", "Needs", "Description about the opportunity", 3));
        listOpportunity.add(new Opportunity("", "Name of the NGO", "Place", "Needs", "Description about the opportunity", 3));
        listOpportunity.add(new Opportunity("", "Name of the NGO", "Place", "Needs", "Description about the opportunity", 3));
        listOpportunity.add(new Opportunity("", "Name of the NGO", "Place", "Needs", "Description about the opportunity", 3));
        listOpportunity.add(new Opportunity("", "Name of the NGO", "Place", "Needs", "Description about the opportunity", 3));
        listOpportunity.add(new Opportunity("", "Name of the NGO", "Place", "Needs", "Description about the opportunity", 3));
        listOpportunity.add(new Opportunity("", "Name of the NGO", "Place", "Needs", "Description about the opportunity", 3));
        listOpportunity.add(new Opportunity("", "Name of the NGO", "Place", "Needs", "Description about the opportunity", 3));
        listOpportunity.add(new Opportunity("", "Name of the NGO", "Place", "Needs", "Description about the opportunity", 3));
        listOpportunity.add(new Opportunity("", "Name of the NGO", "Place", "Needs", "Description about the opportunity", 3));
        listOpportunity.add(new Opportunity("", "Name of the NGO", "Place", "Needs", "Description about the opportunity", 3));
        listOpportunity.add(new Opportunity("", "Name of the NGO", "Place", "Needs", "Description about the opportunity", 3));
        listOpportunity.add(new Opportunity("", "Name of the NGO", "Place", "Needs", "Description about the opportunity", 3));
        listOpportunity.add(new Opportunity("", "Name of the NGO", "Place", "Needs", "Description about the opportunity", 3));

    }

    public void initViews(){
        rvOpportunities = (RecyclerView) findViewById(R.id.rvOpportunities);
    }
}
