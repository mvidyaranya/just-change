package uppercutlabs.justchange.activities;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;

import com.gordonwong.materialsheetfab.MaterialSheetFab;

import java.util.ArrayList;

import uppercutlabs.justchange.R;
import uppercutlabs.justchange.adapters.EntryAdapter;
import uppercutlabs.justchange.fragments.CausesFragment;
import uppercutlabs.justchange.fragments.FilterFragment;
import uppercutlabs.justchange.utils.EntryItem;
import uppercutlabs.justchange.utils.Fab;
import uppercutlabs.justchange.adapters.ViewPagerAdapter;
import uppercutlabs.justchange.utils.Item;
import uppercutlabs.justchange.utils.SectionItem;

public class DonateActivity extends AppCompatActivity {

    private TabLayout tlDonate;
    private ViewPager vpDonate;
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private MaterialSheetFab<Fab> materialSheetFab;
    ArrayList<Item> items = new ArrayList<Item>();
    ListView listview=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donate);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);




        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.logo);

        vpDonate = (ViewPager) findViewById(R.id.vpDonate);

        Fab fab = (Fab) findViewById(R.id.fab);
        View sheetView = findViewById(R.id.fab_sheet);
        View overlay = findViewById(R.id.overlay);
        int sheetColor = getResources().getColor(R.color.white);
        int fabColor = getResources().getColor(R.color.colorAccent);

        // Initialize material sheet FAB
        materialSheetFab = new MaterialSheetFab<>(fab, sheetView, overlay,
                sheetColor, fabColor);



        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new CausesFragment(), "Causes");
        adapter.addFragment(new CausesFragment(), "NGO");
//        adapter.addFragment(new TwoFragment(), "TWO");
//        adapter.addFragment(new ThreeFragment(), "THREE");
        vpDonate.setAdapter(adapter);
//        tlDonate.setupWithViewPager(vpDonate);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(vpDonate);

        listview=(ListView)findViewById(R.id.listView_main);

        items.add(new SectionItem("Donate", R.drawable.icon18));
        items.add(new EntryItem("Toys"));
        items.add(new EntryItem("Clothes"));
        items.add(new EntryItem("Cash"));

        items.add(new SectionItem("To", R.drawable.icon19));
        items.add(new EntryItem("Children"));
        items.add(new EntryItem("Senior Citizen"));
        items.add(new EntryItem("Citizen"));

        EntryAdapter entryAdapter= new EntryAdapter(this, items);
        listview.setAdapter(entryAdapter);

    }

    @Override
    public void onBackPressed() {
        if (materialSheetFab.isSheetVisible()) {
            materialSheetFab.hideSheet();
        } else {
            super.onBackPressed();
        }
    }
}
