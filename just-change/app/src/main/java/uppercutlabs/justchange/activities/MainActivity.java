package uppercutlabs.justchange.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import uppercutlabs.justchange.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void donateClicked(View v){
        Intent openDonateActivity = new Intent(MainActivity.this, DonateActivity.class);
        startActivity(openDonateActivity);
    }

    public void volunteerClicked(View v){
        Intent openActivity = new Intent(MainActivity.this, VolunteerActivity.class);
        startActivity(openActivity);
    }

    public void internshipClicked(View v){
        Intent openActivity = new Intent(MainActivity.this, InternshipActivity.class);
        startActivity(openActivity);
    }
}
