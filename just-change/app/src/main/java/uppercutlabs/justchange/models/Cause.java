package uppercutlabs.justchange.models;

/**
 * Created by hp1 on 3/15/2016.
 */
public class Cause {
    String imageUrl, name, description;
    int notificationCount;

    public Cause(String imageUrl, String name, String description, int notificationCount) {
        this.imageUrl = imageUrl;
        this.name = name;
        this.description = description;
        this.notificationCount = notificationCount;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNotificationCount() {
        return notificationCount;
    }

    public void setNotificationCount(int notificationCount) {
        this.notificationCount = notificationCount;
    }
}
