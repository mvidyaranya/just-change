package uppercutlabs.justchange.models;

/**
 * Created by hp1 on 3/22/2016.
 */
public class Opportunity {

    String imageUrl, name, place, needs, description;
    int notificationCount;

    public Opportunity(String imageUrl, String name, String place, String needs, String description, int notificationCount) {
        this.imageUrl = imageUrl;
        this.name = name;
        this.place = place;
        this.needs = needs;
        this.description = description;
        this.notificationCount = notificationCount;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getNeeds() {
        return needs;
    }

    public void setNeeds(String needs) {
        this.needs = needs;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNotificationCount() {
        return notificationCount;
    }

    public void setNotificationCount(int notificationCount) {
        this.notificationCount = notificationCount;
    }
}
