package uppercutlabs.justchange.models;

/**
 * Created by hp1 on 3/20/2016.
 */
public class Event {

    String imageUrl, name, place, date;
    int notificationCount;

    public Event(String imageUrl, String name, String place, String date, int notificationCount) {
        this.imageUrl = imageUrl;
        this.name = name;
        this.place = place;
        this.date = date;
        this.notificationCount = notificationCount;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getNotificationCount() {
        return notificationCount;
    }

    public void setNotificationCount(int notificationCount) {
        this.notificationCount = notificationCount;
    }
}
