package uppercutlabs.justchange.models;

/**
 * Created by vidyaranya on 20/3/16.
 */
public class CauseFilter {
    String imageUrl, name;

    public CauseFilter(String imageUrl, String name) {
        this.imageUrl = imageUrl;
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
