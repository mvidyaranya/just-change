package uppercutlabs.justchange.utils;

/**
 * Created by Malladi-Bhai on 3/22/2016.
 */
public interface Item {
    public boolean isSection();
}
