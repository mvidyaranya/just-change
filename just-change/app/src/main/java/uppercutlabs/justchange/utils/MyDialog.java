package uppercutlabs.justchange.utils;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Window;

import java.util.ArrayList;
import java.util.List;

import uppercutlabs.justchange.R;
import uppercutlabs.justchange.adapters.CauseFilterAdapter;
import uppercutlabs.justchange.models.CauseFilter;

/**
 * Created by vidyaranya on 21/3/16.
 */
public class MyDialog extends Dialog {

    Context context;

    RecyclerView rvCauses;
    LinearLayoutManager llManager;
    List<CauseFilter> listCauses;
    CauseFilterAdapter causeAdapter;

    public MyDialog(Context context) {
        super(context);
    }


    public myOnClickListener myListener;

    // This is my interface //
    public interface myOnClickListener {
        void onButtonClick();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.fragment_filter);
        llManager = new LinearLayoutManager(context);
        rvCauses = (RecyclerView) findViewById(R.id.listFilter);
        rvCauses.setLayoutManager(llManager);

        //set up dummy list
        initDummyCauses();
        causeAdapter = new CauseFilterAdapter(listCauses, context);
        rvCauses.setAdapter(causeAdapter);
    }

    private void initDummyCauses() {
        listCauses = new ArrayList<>();

        listCauses.add(new CauseFilter("imageUrl", "Cause Name"));
        listCauses.add(new CauseFilter("imageUrl", "Cause Name"));
        listCauses.add(new CauseFilter("imageUrl", "Cause Name"));
        listCauses.add(new CauseFilter("imageUrl", "Cause Name"));
        listCauses.add(new CauseFilter("imageUrl", "Cause Name"));
        listCauses.add(new CauseFilter("imageUrl", "Cause Name"));
        listCauses.add(new CauseFilter("imageUrl", "Cause Name"));
        listCauses.add(new CauseFilter("imageUrl", "Cause Name"));
        listCauses.add(new CauseFilter("imageUrl", "Cause Name"));
        listCauses.add(new CauseFilter("imageUrl", "Cause Name"));
        listCauses.add(new CauseFilter("imageUrl", "Cause Name"));
        listCauses.add(new CauseFilter("imageUrl", "Cause Name"));
        listCauses.add(new CauseFilter("imageUrl", "Cause Name"));
        listCauses.add(new CauseFilter("imageUrl", "Cause Name"));
        listCauses.add(new CauseFilter("imageUrl", "Cause Name"));
        listCauses.add(new CauseFilter("imageUrl", "Cause Name"));
        listCauses.add(new CauseFilter("imageUrl", "Cause Name"));
        listCauses.add(new CauseFilter("imageUrl", "Cause Name"));


    }

}