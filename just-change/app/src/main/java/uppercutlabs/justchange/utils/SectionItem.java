package uppercutlabs.justchange.utils;

/**
 * Created by Malladi-Bhai on 3/22/2016.
 */
public class SectionItem implements Item{

    private final String title;
    private final Integer image;

    public Integer getImage() {
        return image;
    }

    public SectionItem(String title, Integer image) {
        this.title = title;
        this.image = image;
    }

    public String getTitle(){
        return title;
    }

    @Override
    public boolean isSection() {
        return true;
    }

}